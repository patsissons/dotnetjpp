﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DigitalHarmoniX.DotNETJPP
{
    public static class Parser
    {
        public const string KeyValuePattern = @"^\s*([^#!]([^\\ =:]*(\\.)?)*)\s*[ =:]\s*(.*)$";
        public const string EscapePattern = @"\\(.)";

        public static Dictionary<string, string> ParseFile(string path)
        {
            Dictionary<string, string> properties = null;

            var file = new FileInfo(path);

            if (file.Exists)
            {
                properties = ParseFile(file);
            }
            else
            {
                throw new FileNotFoundException("File Not Found", path);
            }

            return properties;
        }

        public static Dictionary<string, string> ParseFile(FileInfo file)
        {
            Dictionary<string, string> properties = null;

            using (var fs = file.OpenRead())
            {
                properties = ParseStream(fs);
            }

            return properties;
        }

        public static Dictionary<string, string> ParseText(string text)
        {
            Dictionary<string, string> properties = null;

            using (var stream = new MemoryStream())
            using (var sw = new StreamWriter(stream))
            {
                sw.Write(text);
                sw.Flush();

                stream.Seek(0, SeekOrigin.Begin);

                properties = ParseStream(stream);
            }

            return properties;
        }

        public static Dictionary<string, string> ParseStream(Stream stream)
        {
            Dictionary<string, string> properties = new Dictionary<string,string>();

            using (var sr = new StreamReader(stream))
            {
                var keyValueRegex = new Regex(KeyValuePattern, RegexOptions.Compiled);
                var escapeRegex = new Regex(EscapePattern, RegexOptions.Compiled);

                while (true)
                {
                    var line = sr.ReadLine();

                    if (line == null)
                    {
                        break;
                    }

                    line = line.TrimStart();

                    if (line.TrimEnd().Length == 0)
                    {
                        continue;
                    }

                    var match = keyValueRegex.Match(line);

                    if (match.Success)
                    {
                        var key = match.Groups[1].Value.Trim();
                        var value = match.Groups[4].Value.Trim();

                        key = escapeRegex.Replace(key, "$1");

                        while (string.IsNullOrEmpty(line) == false && IsMultiLine(line))
                        {
                            value = value.TrimEnd('\\');

                            line = sr.ReadLine().TrimEnd();

                            value += Environment.NewLine + line;
                        }

                        value = escapeRegex.Replace(value.Trim(), "$1");

                        if (properties.ContainsKey(key))
                        {
                            properties[key] = value;
                        }
                        else
                        {
                            properties.Add(key, value);
                        }
                    }
                }
            }

            return properties;
        }

        private static bool IsMultiLine(string input)
        {
            return input.TrimEnd().LastOrDefault() == '\\';
        }
    }
}
